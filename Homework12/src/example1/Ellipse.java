package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// овал
public class Ellipse extends Figure {

    private double r1;
    private double r2;

    public Ellipse(double x, double y, double r1, double r2) {
        super(x, y);
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI * r1 * r2 + Math.pow((r1 - r2), 2)) / (r1 + r2);
    }
}
