package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// прямоугольник
public class Rectangle extends Figure {
    // две стороны, потому что другие две - им равны
    private int a;
    private int b;

    public Rectangle(double x, double y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }
}
