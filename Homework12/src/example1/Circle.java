package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// круг
public class Circle extends Ellipse {
    public Circle(double x, double y, double r) {
        super(x, y, r, r);
    }
}
