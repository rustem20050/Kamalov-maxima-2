package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4, 9, 11, 15);
        Square square = new Square(5, 7, 10);
        Circle circle = new Circle(6, 0, 21);
        Ellipse ellipse = new Ellipse(8, 13, 3, 10);

        Figure [] figure = {rectangle, square, circle, ellipse};

        for (int i = 0; i < figure.length; i++) {
            System.out.println(figure[i].getPerimeter());
        }

        for (int i = 0; i < figure.length; i++) {
            figure[i].move(66, 77);
        }

        for (int i = 0; i < figure.length; i++) {
            System.out.println(figure[i].getX() + " " + figure[i].getY());
        }

//        rectangle.move(2 ,2);
//        square.move(3, 5);
//        circle.move(4, -2);
//        ellipse.move(6, 11);
    }
}
