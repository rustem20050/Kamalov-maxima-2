//Написать рекурсивную функцию int sumOfDigits(int number) - возвращает сумму цифр числа.
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.print(sumOfDigits(number));

    }

    public static int sumOfDigits(int number) {
        int sumOfDigit = 0;
        int n;
        while (number != 0) {
            n = number % 10;
            number /= 10;
            sumOfDigit += n;

        }
        return sumOfDigit;
    }
}
