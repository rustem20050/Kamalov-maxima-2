package ru.maxima.collections;

import java.util.ArrayList;

/**
 * 18.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SetHashImpl<E> implements Set<E> {

    // пустой объект
    public static Integer number = 1;

    public static ArrayList word = new ArrayList();

    private MapHashImpl<E, Object> hashMap;

    public SetHashImpl() {
        this.hashMap = new MapHashImpl<>();
    }

    @Override
    public void add(E element) {

        if (contains(element)) {
            number = (int) retval(element) + 1;

        } else {
            number = 1;
            word.add(element);
        }
        this.hashMap.put(element, number);
    }

    public void print() {
        int max = 0;
        int indexOfMax = 0;

        for (int i = 0; i < word.size(); i++) {
            indexOfMax = i;
            max = (int) retval((E) word.get(i));
            for (int j = i; j < word.size(); j++) {
                if ((int) retval((E) word.get(j)) > max) {
                    max = (int) retval((E) word.get(j));
                    indexOfMax = j;
                }
            }
            break;
        }
        System.out.println(word.get(indexOfMax) + " " + max);
    }

    @Override
    public boolean contains(E element) {
        return hashMap.containsKey(element);
    }

    public Object retval(E element) {
        return this.hashMap.get(element);
    }
}
