package ru.maxima;

import ru.maxima.collections.Map;
import ru.maxima.collections.MapHashImpl;
import ru.maxima.collections.Set;
import ru.maxima.collections.SetHashImpl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char characters[];

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                char character = (char)currentByte;
                characters[position] = character;
                position++;
                currentByte = inputStream.read();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String text = new String(characters);
        text.toLowerCase();
        String words[] = text.split(" ");
        Set<String> set = new SetHashImpl<>();
        for (int i = 0; i < words.length; i++) {
            set.add(words[i]);
        }
        set.print();
    }
}