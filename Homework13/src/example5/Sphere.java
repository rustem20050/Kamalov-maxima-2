package example5;

/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Sphere implements Shape, SpaceObject {
    // координаты центра
    private double x;
    private double y;
    private double z;

    private double radius;

    public Sphere(double x, double y, double z, double radius) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.radius = radius;
    }

    @Override
    public void scale(double value) {
        this.radius *= value;
    }

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ + newX;
    }
}
