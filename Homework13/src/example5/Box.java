package example5;

/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// трехмерный ящик
public class Box implements Shape, SpaceObject {
    // координаты центра
    private double x;
    private double y;
    private double z;
    // длина, ширина, высота
    private double length;
    private double height;
    private double width;

    public Box(double x, double y, double z, double length, double height, double width) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.length = length;
        this.height = height;
        this.width = width;
    }

    @Override
    public void scale(double value) {
        this.length *= value;
        this.height *= value;
        this.width *= value;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY + newZ;
        this.z = newZ;
    }
}
