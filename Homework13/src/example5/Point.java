package example5;

/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Point implements SpaceObject {
    // координаты центра
    private double x;
    private double y;
    private double z;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX + newZ;
        this.y = newY;
        this.z = newZ;
    }
}
