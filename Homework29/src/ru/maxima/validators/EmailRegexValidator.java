package ru.maxima.validators;

import ru.maxima.validators.exceptions.EmailValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailRegexValidator implements EmailValidator{

    private final Pattern regexPattern;

    public EmailRegexValidator(String regex) {
        this.regexPattern = Pattern.compile(regex);
    }

    @Override
    public void validate(String email) throws EmailValidationException {
        Matcher matcher = regexPattern.matcher(email);
        if(!matcher.matches()){
            throw new EmailValidationException();
        }

    }
}
