package ru.maxima;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.maxima.services.UsersService;
import ru.maxima.validators.EmailValidator;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        UsersService usersService = context.getBean(UsersService.class);
        usersService.signUp("User2", "User2", "user3@gmail", "Qwerty007");
    }
}
