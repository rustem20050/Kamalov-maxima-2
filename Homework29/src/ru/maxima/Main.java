package ru.maxima;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.maxima.repositories.UsersRepository;
import ru.maxima.repositories.UsersRepositoryNamedParameterJdbcTemplateImpl;
import ru.maxima.services.UsersService;
import ru.maxima.services.UsersServiceImpl;
import ru.maxima.validators.EmailFormatValidator;
import ru.maxima.validators.PasswordLengthValidator;
import ru.maxima.validators.PasswordValidator;

/**
 * 26.03.2022
 * 29. Console Application V2
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setUsername("postgres");
        config.setPassword("Qwerty123");
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/maxima_2");
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        UsersRepository usersRepository = new UsersRepositoryNamedParameterJdbcTemplateImpl(dataSource);
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        EmailFormatValidator emailFormatValidator = new EmailFormatValidator();

        UsersService usersService = new UsersServiceImpl(usersRepository, emailFormatValidator, passwordValidator);

        usersService.signUp("temp_FirstName", "temp_LastName", "email@mail.ru", "qwerty123");
    }
}
