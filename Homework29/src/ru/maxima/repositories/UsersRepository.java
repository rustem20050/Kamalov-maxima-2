package ru.maxima.repositories;

import ru.maxima.models.User;

import java.util.List;
import java.util.Optional;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository {
    void save(User user);

    Optional<User> findOneByEmail(String email);

    Optional<User> findById(Long id);

    List<User> findAllByHasLicense(boolean hasLicense);

    void update(User user);
}
