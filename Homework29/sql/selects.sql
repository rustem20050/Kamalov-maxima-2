-- получить всех водителей
select *
from account;

-- получить всех водителей в отсортированном виде (по возрастанию id)
select *
from account
order by id;

-- получить всех водителей по убыванию их опыта вождения, если опыта нет нескольких - их по наличию прав
select *
from account
order by experience desc, has_license desc;

-- получить имена тех, у кого нет прав
select first_name
from account
where has_license = false;

-- получить имена и опыт тех, у кого опыт больше 10-ти лет, но меньше 20 лет
select first_name, experience
from account
where experience between 10 and 20;

-- получить количество людей без прав
select count(*)
from account
where has_license = false;

-- получить количество водителей с лицензией и без лицензии
select has_license, count(*)
from account
group by has_license;

-- получить количество людей с определенным стажем
select experience, count(*)
from account
group by experience;

-- получить все варианты стажа из таблицы (уникальные значения колонки experience)
select distinct experience
from account;

-- получить средний стаж всех водителей, имеющих лицензию
select avg(experience)
from account
where has_license = true;

-- получить всех владельцев, у которых есть хотя бы одна машина
select id, first_name
from account
where id in (select distinct owner_id from car where owner_id notnull)
order by id;

-- получить владельцев, у которых есть хотя бы одна машина красного цвета
select id, first_name
from account
where id in (select owner_id from car where color = 'Red');

-- получить имена владельцев, у которых более одной машины
select id, first_name
from account
where id in (select owner_id
             from (select owner_id, count(id) as cars_count from car where owner_id notnull group by owner_id) as counts
             where counts.cars_count > 1);

-- возвращает владельцев и количество их машин
select owner_id, count(id) as cars_count
from car
where owner_id notnull
group by owner_id;

-- получить имя владельца которую арендуют более 2-х человек
select first_name
from account
where id in (select owner_id
             from car
             where id in (
                 select car_id
                 from (select car_id
                            , count(driver_id) as driver_count
                       from driver_car
                       group by (car_id)) as cars
                 where cars.driver_count
                           > 1));

-- JOIN - пересечение двух таблиц по заданному условию
-- LEFT - выборка состоит из всех элементов левой таблицы и пересечения с правой таблицей
-- RIGHT - выборка состоит из всех элементов правой таблицы и пересечения с левой таблицей
-- INNER - выборка состоит ТОЛЬКО из элементов-пересечения двух таблиц
-- FULL OUTER - выборка состоит из всех элементов двух таблиц

-- получить всех владельцев и их машины (получили всех владельцев, включая тех, у кого нет машин)
select *
from account a
         left join car c on a.id = c.owner_id;

-- получить всех владельцев и их машины (получили все машины, включая те, у кого нет владельцев)
select *
from account a
         right join car c on a.id = c.owner_id;

-- получить всех владельцев и их машины (получили только тех владельцев, у которых есть машины)
select *
from account a
         inner join car c on a.id = c.owner_id;

-- получить всех владельцев и их машины (получили всех владельцев и все машины)
select *
from account a
         full outer join car c on a.id = c.owner_id;

-- TODO: убрать дубликаты
-- получить машины одинакового цвета
select distinct on (a.id) *
from car a
         inner join car b on a.color = b.color
where a.id != b.id;