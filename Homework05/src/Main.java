//На вход подается список возрастов людей (мин 0 лет, макс 120 лет)
//Необходимо вывести информацию о том, какой возраст чаще всего встречается.


public class Main {

    public static void main(String[] args) {
	int[] ages = {65, 32, 82, 68, 12, 44, 32, 109, 82, 82};
	int[] humans = new int[120];
	int indexOfMax = 0;
	for (int i=0; i < ages.length; i++) {
		int count = ages[i];
		humans[count]++;
	}

	for (int i=0; i < humans.length; i++) {
		indexOfMax = i;
		int max = humans[i];
		for (int j=i; j < humans.length; j++) {
			if (humans[j] > max) {
				max = humans[j];
				indexOfMax = j;
				}

		}
		break;
	}
		System.out.println("Возраст " + indexOfMax + " встречается чаще всего");
    }
}
