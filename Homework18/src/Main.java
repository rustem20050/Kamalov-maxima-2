import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.Entry.comparingByValue;

//Считать из файла текст (ТОЛЬКО НА АНГЛИЙСКОМ), выяснить, какие слова встречаются чаще других и вывести их на экран.

public class Main {

    public static void main(String[] args) {
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char characters[] = null;
            try {
                characters = new char[inputStream.available()];
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

            int position = 0;
            try {
                int currentByte = inputStream.read();

                while (currentByte != -1) {
                    char character = (char) currentByte;
                    if (Character.isLetter(character) | character == 32) {
                        characters[position] = character;
                        position++;
                        currentByte = inputStream.read();
                    } else {
                        characters[position] = (char) 32;
                        position++;
                        currentByte = inputStream.read();
                    }
                }

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

        // создаем строку на основе массива
        String text = new String(characters).replaceAll("  ", " ").toLowerCase();
        String words[] = text.split(" ");
        System.out.println(text);

        Map<String, Integer> counts = new HashMap<>();
        // пробегаемся по массиву всех слов
        for (String word : words) {
            if(!word.isEmpty()) {
                Integer count = counts.get(word);
                // если слово попадается в тексте первый раз
                if (count == null) {
                    // присваиваем переменной count значение 0
                    count = 0;
                }
                count++;
                int result = count;
                counts.put(word, result);
            }
        }

        counts.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(System.out::println);

    }
}
