

public class LinkedList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    private int size = 0;

    public void add(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public int get(int index) {
        // начинаем с первого элемента
        Node current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return -1;
    }

    public void removeAt(int index) {
        Node previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void removeold(int element) { //первая версия

        Node current = first;

        for (int i = 0; i < size; i++) {

            if (i == 0 && current.value == element) {
                first = first.next;
                size--;
                return;
            }

            if (current.value == element) {

                Node previousRemove = first;

                for (int j = 0; j < (i - 1); j++) {
                    previousRemove = previousRemove.next;
                }

                if (i == (size - 1)) {
                    last = previousRemove;
                    return;
                }

                Node forRemove = previousRemove.next;
                previousRemove.next = forRemove.next;
                current = current.next;
                size--;

            } else {
                current = current.next;
            }
        }
    }

    public void print() {
        Node current = first;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }
        System.out.println();
    }

    public void remove(int element) {

        Node current = first;

        if (first.value == element) {
            first = first.next;
            size--;
            return;
        }

        for (int i = 0; i < size - 1; i++) {

            if (current.next.value == element) {

                if (i == (size - 1)) {
                    last = current;
                    size--;
                    return;
                }

                Node forRemove = current.next;
                current.next = forRemove.next;
                size--;
                return;
            } else {
                current = current.next;
            }

        }
    }

    public void removeLast() {
        reverse();
        first = first.next;
        size--;
        reverse();
    }

        public void removeAll(int element) {
            Node current = first;

            if (first.value == element) {
                first = first.next;
                size--;
            }

            for (int i = 0; i < size - 1; i++) {

                if (current.next.value == element) {

                    if (i == (size - 1)) {
                        last = current;
                        size--;
                        return;
                    }

                    Node forRemove = current.next;
                    current.next = forRemove.next;
                    size--;
                } else {
                    current = current.next;
                }
            }
        }

    public void add(int index, int element) {
        Node newNode = new Node(element);
        Node current = first;
        if  (index == 0) {
            newNode.next = first;
            first = newNode;
            return;
        }

        if (index == size) {
            last.next = newNode;
            last = newNode;
            return;
        }

        for (int i = 0; i < size; i++) {
            if (i == index - 1) {
                Node temp = current.next;
                current.next = newNode;
                newNode.next = temp;
                size++;
                return;
            }
            else {current = current.next;}
        }

    }

    public void reverse() {
        Node current = first;
        Node reversedCurrent = null;
        while (current != null) {
            Node next = current.next;
            current.next = reversedCurrent;
            reversedCurrent = current;
            current = next;
        }
        first = reversedCurrent;
    }
}

