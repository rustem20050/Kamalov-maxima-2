package nodes;

/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Node a = new Node(5);
        Node b = new Node(70);
        Node c = new Node(88);
        Node d = new Node(90);

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);

        Node current = a;

        while (current != null) {
            System.out.println(current.getValue());
            current = current.getNext();
        }

        int i = 0;
    }
}
