// Реализовать в Program процедуру void selectionSearch
// и функцию booleanSearch с бинарным поиском.
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] array = {87, 56, 44, 88, 36, 18, 4, 3, -2};
        Scanner scanner = new Scanner(System.in);
        int element = scanner.nextInt();
        selectionSearch(array);
        System.out.println(Arrays.toString(array));
        System.out.println(booleanSearch(array, element));
    }

    public static boolean booleanSearch(int[] array, int element) {
        boolean hasElement = false;
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
                middle = left + (right - left) / 2;
            } else if (element > array[middle]) {
                left = middle + 1;
                middle = left + (right - left) / 2;
            } else {
                hasElement = true;
                break;
            }
        }
        return hasElement;
    }

    public static void selectionSearch(int[] array) {
        int min, indexIfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexIfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexIfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexIfMin];
            array[indexIfMin] = temp;
        }
    }
}

