--удаление таблицы driver
drop table if exists food_stuffs;

--создание таблицы ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ
create table food_stuffs
(
    id serial primary key,
    product_name char(40), --"Название товара"
    price double precision not null check (price >= 0), --"Стоимость"
    expiration_date date, --"Срок годности"
    product_receipt date, --"Дата поступления товара"
    supplier char(20) --"Название поставщика"
);

--внесение данных в таблицу ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Хлеб Сельский', '34.00', '20220203', '20220201', 'Хлебозавод №3');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Корж', '13.00', '20220215', '20220201', 'Хлебозавод №3');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Кекс с изюмом', '16.00', '20220210', '20220125', 'Хлебозавод №3');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Батон нарезной', '29.00', '20220201', '20220125', 'Хлебозавод №3');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Молоко', '60.00', '20220301', '20220201', 'Просто молоко');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Творог', '90.00', '20220215', '20220201', 'Просто молоко');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Сметана', '53.00', '20220208', '20220201', 'Просто молоко');
insert into food_stuffs
(product_name, price, expiration_date, product_receipt, supplier)
values ('Чак-чак', '165.00', '20220210', '20220110', 'БКК');

--удалить строку с id = 1
delete from food_stuffs where id = 1;

--Обновление строк
update food_stuffs set price = '34.90' where id = 1;

--добавление колонки
alter table food_stuffs add Overdue boolean default false;

--получить весь список
select * from food_stuffs;

--получить список с отсортировонном по id виде
select *from food_stuffs order by id;

--получить товары по убыванию срока годности
select * from food_stuffs order by expiration_date desc;

-- Получить товары, которые дороже 100 рублей
select product_name, price from food_stuffs where price > '100.00';

-- Получить товары, которые были поставлены ранее 02-02-2020 года
select product_name, product_receipt from food_stuffs where product_receipt < '20220202';

-- Получить названия всех поставщиков *
select distinct supplier from food_stuffs;