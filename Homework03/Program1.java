import java.util.Scanner;
//необходимо найти число, у которой сумма цифр меньше всех остальных
class Program1 {
public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	int number = scanner.nextInt();
	int min = 99999;
	int digitSum = 0;
	int result = 0;

	while (number != -1) {
			int temp = number;
		while (number != 0) {
			int temp1 = number % 10;
			number = number / 10;
			digitSum = temp1 + digitSum;
			if (digitSum < min) {
				min = digitSum;
				result = temp;
			}
			digitSum = 0;
		}
		number = scanner.nextInt();	
}
System.out.println(result);
}
}

