import java.util.Scanner;
//необходимо найти количество локальных минимумов
class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nextNumber = scanner.nextInt();
		int lastNumber = 0;
		int number = 0;
		int kol = 0;
		String result = "";

		while (nextNumber != -1) {
			boolean lastNum = (lastNumber > number);
			boolean nextNum = (nextNumber > number);
			if (lastNum == true && nextNum == true) {
				result = result + "" + number + " ";
				kol++;
			}
			lastNumber = number;
			number = nextNumber;
			nextNumber = scanner.nextInt();
		}
		System.out.println(result);
		System.out.println(kol);
	}
}
