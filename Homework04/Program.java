import java.util.Scanner;
import java.util.Arrays;

//Объявить массив (размер - 10). Заполнить его элементами из консоли.
//Вывести в обратном порядке.

class Program {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int [] array = new int[10];

		for (int i=0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			
		}
//		System.out.println(Arrays.toString(array));
		for (int i=array.length - 1; i>=0; i--) {	
		System.out.print(array[i] + " ");
		}
	}
}
