import java.util.Arrays;

/**
 * 09.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// список на основе массива
public class ArrayList<T> {
    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private T[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = (T[])new Object[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     * @param element добавляемый элемент
     */
    public void add(T element) {
        // если у меня переполнен массив
        ensureSize();

        this.elements[size++] = element;
    }

    public void addToBegin(T element) {
        ensureSize();

        for (int i = size; i >= 1; i--) {
            this.elements[i] = this.elements[i - 1];
        }

        this.elements[0] = element;
        size++;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        T[] newArray = (T[])new Object[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получение элемента по индексу
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если такого индекса нет - ошибка
     */
    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("В списке нет такого индекса");
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     * @return размер списка
     */
    public int size() {
        return size;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     *
     * 0 -> [14] 1-> [71] 2-> [82] 3-> [25], size = 4
     *
     * removeAt(1)
     *
     *
     * [14] [82] [25] | [25], size = 3
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        if (index >= 0 && index <= size) {
            T[] newArray = (T[])new Object[elements.length - 1];
            for (int i = 0; i < newArray.length; i++) {
                newArray[i] = elements[i];
            }
            for (int i = index; i < newArray.length; i++) {
                newArray[i] = this.elements[i + 1];
            }
            this.elements = newArray;
            size--;
        }
        else System.out.println("Не нашел.");
        return;
    }

    /**
     * Удаляет первое вхождение элемента в список
     *
     * 34, 56, 78, 56, 92, 11
     *
     * remove(56)
     *
     * 34, 78, 56, 92, 11
     *
     * @param element
     */
    public void remove(T element) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     *
     * 34, 56, 78, 56, 92, 11
     *
     * removeLast(56)
     *
     * 34, 56, 78, 92, 11
     *
     * @param element
     */
    public void removeLast(T element) {
        revers(elements);
        remove(element);
        revers(elements);
    }

    public void removeAll(T element) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                removeAt(i);

            }
        }
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент, который стоял под индексом index сдвигается вправо (как и все остальные элементы)
     * 34, 56, 78, 56, 92, 11
     *
     * add(2, 100)
     *
     * 34, 56, 100, 78, 56, 92, 11
     *
     * @param index куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, T element) {
        if (index >= 0 && index < size) {
            T[] newArray = (T[])new Object[elements.length + 1];
            for (int i = 0; i < newArray.length; i++) {
                if(index == i) {
                    newArray[i] = element;
                }
            }

        }
    }

    public void print() {
            System.out.println(Arrays.toString(elements));
    }

    private void revers(T[] array) {
        T[] newArray = (T[])new Object[elements.length];
        for (int i = 0; i < elements.length; i++) {
            newArray[i] = elements[elements.length - 1 - i];
        }
        this.elements = newArray;
    }
}
