import java.io.*;
//Реализовать класс TextConverter, в котором будет метод void toLowerCaseAll(String sourceFileName, String targetFileName).
//
//Данный метод принимает на вход два имени файла.
//В первом (исходном) файле дан текст на английском языке (и еще могут быть символы, но все гарантированно не Unicode).
//Метод считывает весь текст, все большие буквы делает маленькими и убирает все "небуквенные символы", кроме пробелов.
//Весь этот текст записывает в целевой файл с названием targetFileName
//
//Hello! How are you?
//
//Результат:
//
//hello how are you
//
//Можно использовать
//
//Character.isLetter();
//Character.toLowerCase();
public class TextConverter {
    public static void toLowerCaseAll(String sourceFileName, String targetFileName)  {
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(sourceFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char characters[];

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                char character = (char) currentByte;
                if (Character.isLetter(character) | character == 32) {
                    characters[position] = character;
                    position++;
                    currentByte = inputStream.read();
                } else {
                    characters[position] = (char) 32;
                    position++;
                    currentByte = inputStream.read();
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String text = new String(characters).replaceAll("  ", " ").toLowerCase();
        System.out.println(text);

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(targetFileName);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        byte[] bytes = text.getBytes();

        try {
            outputStream.write(bytes);
            outputStream.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

