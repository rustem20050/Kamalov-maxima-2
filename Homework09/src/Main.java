//На базе проекта 12.Bus добавить функциональность:
//
//у водителя сделать метод `drive`, который заставит "поехать" автобус.
//Когда автобус едет, все его пассажири называют свои имена.
//Когда автобус едет, нельзя менять водителя, нельзя, чтобы пассажиры покидали свои места,
//нельзя, чтобы автобус принимал пассажиров.
public class Main {

    public static void main(String[] args) {
        Driver driver = new Driver("Семен", 10);
        Driver driver1 = new Driver("Артем", 3);
        Bus bus = new Bus(99, "Нефаз", 5);

        Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Айрат");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");

//        passenger1.goToBus(bus);
//        passenger2.goToBus(bus);
//        passenger3.goToBus(bus);
//        passenger4.goToBus(bus);
//        passenger5.goToBus(bus);
//        passenger6.goToBus(bus);
//

        bus.setDriver(driver);
        driver.drive();
        passenger1.goToBus(bus);
        passenger1.leaveBus(bus);
        driver.drive();
        passenger1.goToBus(bus);
        passenger1.leaveBus(bus);

//
//
//
//        int i = 0;

    }
}
