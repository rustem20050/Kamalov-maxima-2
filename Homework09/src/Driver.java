public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public String getName() {
        return name;
    }

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive() {
        if (this.bus != null) {
            this.bus.setRide();
            this.bus.callNames();
        }
        else {
            System.out.println("Водитель бежит сзади автобуса");
        }
    }

}

