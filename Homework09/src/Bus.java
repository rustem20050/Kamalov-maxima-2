public class Bus {
    private int number;
    private String model;
    private Driver driver;
    private boolean ride = false;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (this.ride) {
            System.out.println("Автобус едет, посадка запрещена");
            return;
        }
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void exitPassenger(Passenger passenger) {
        if (this.ride) {
            System.out.println("Автобус едет, вставать запрещено");
            return;
        }
        this.passengers[count] = null;
        this.count--;
    }

    public void setDriver(Driver driver) {
        if (this.ride == false) {
            this.driver = driver;
            driver.setBus(this);
            System.out.println("Ваш водитель: " + driver.getName());
        }
        else System.out.println("Нельзя менять водителя по время поездки.");
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public void setRide() {
        if (!this.ride) {
            System.out.println("Поехали!");
            this.ride = true;
        }
        else {System.out.println("Остановились.");
            this.ride = false;
        }
    }

    public boolean isRide() {
        return ride;
    }
    public void callNames() {
        for (int i = 0; i < count; i++) {
            System.out.println("Меня зовут " + passengers[i].getName());
        }
    }

//    public boolean getRide() {
//        return this.ride;
//    }

}