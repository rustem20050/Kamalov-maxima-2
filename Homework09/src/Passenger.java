/**
 * 30.10.2021
 * 12. Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Passenger {
    private String name;

    // объектная переменная, поле, которое ссылается на какой-то автобус
    private Bus bus;

    // перегруженные конструкторы
    public Passenger() {
        this.name = "Без имени";
    }

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void goToBus(Bus bus) {
        // проверяем, а не в автобусе ли мы уже?
        if (this.bus != null) {
            System.out.println("Я, " + name + ", уже в автобусе!");
        } else {
            if (!bus.isFull() && (!bus.isRide())) {
                // если автобуса еще не было
                this.bus = bus;
                // передаем в автобус себя
                this.bus.incomePassenger(this);
                System.out.println("Я, " + name + ", сел в автобус");
            } else {
                System.out.println("Я, " + name + ", не попал в автобус(");
                System.err.println();
            }
        }
    }


    public void leaveBus(Bus bus) {
        if (this.bus != null) {
            this.bus.exitPassenger(this);
            System.out.println("Я, " + name + ", вышел из автобуса");
        }
        else {
            System.out.println("Я, " + name + ", и так стою на улице");
        }
    }
}
