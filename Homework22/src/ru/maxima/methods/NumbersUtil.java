package ru.maxima.methods;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersUtil {

    private double number;

    public NumbersUtil(double number) {
        this.number = number;
    }

    // boolean method(int)
    public static boolean isPrime(int number) {

        if (number == 2 || number == 3) {
            return true;
        }

        // 37 -> 2, 3, 4, 5, 6

        // перебираем делители числа, начиная с двух и до корня из этого числа
        // i < sqrt(number)
        // i * i < sqrt(number) * sqrt(number)
        // i^2 < number
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
    
    public double convert(int number) {
        return number * this.number;
    }
}
