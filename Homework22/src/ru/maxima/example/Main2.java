package ru.maxima.example;

import java.util.stream.Stream;

public class Main2 {

    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.of(4, 5, 10, 16, -1, 20, 46, 71);

        integerStream
                .filter(number -> number % 2 == 0)
                .map(number -> {
                    if (number > 10) {
                        return "Большое число!";
                    } else {
                        return "Маленькое число!";
                    }
                })
                .forEach(System.out::println);
    }
}
