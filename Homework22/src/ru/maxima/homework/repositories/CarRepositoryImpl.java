package ru.maxima.homework.repositories;

import ru.maxima.homework.models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CarRepositoryImpl implements CarRepository {

    private String fileName;

    public CarRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.split("\\|");
        return new Car(parsedLine[0], parsedLine[1], parsedLine[2],
                Integer.parseInt(parsedLine[3]), Double.parseDouble(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage)).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void findByMinPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Цвет авто с минимальной ценой: " + reader.lines().map(toCarMapper)
                    .min(Comparator.comparing(Car::getPrice)).get().getColor());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Car> findByPrice(int by, int to) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getPrice() >= by && car.getPrice() <= to).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void findByAverageCostOfCar(String model) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Средняя стоимость " + model + ": " + reader.lines().map(toCarMapper)
                    .filter(car -> car.getModel().equals(model))
                    .mapToDouble(Car::getPrice).average().getAsDouble());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
