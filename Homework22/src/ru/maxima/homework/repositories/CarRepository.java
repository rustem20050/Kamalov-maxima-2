package ru.maxima.homework.repositories;

import ru.maxima.homework.models.Car;

import java.util.List;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CarRepository {
    List<Car> findAll();

    List<Car> findByColorOrMileage(String color, Integer mileage);

    public void findByMinPrice();

    List<Car> findByPrice(int by, int to);

    public void findByAverageCostOfCar(String model);

}
