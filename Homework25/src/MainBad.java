import java.sql.*;

public class MainBad {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/maxima_2";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "Qwerty123";

    //language=SQL
    private static final String SQL_SELECT_FROM_ACCOUNT = "select * from foodstuffs order by id";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_FROM_ACCOUNT);

            while (resultSet.next()) {
                String productName = resultSet.getString("productname");
                String data = resultSet.getString("receiptdate");
                boolean overdue = resultSet.getBoolean("overdue");

                System.out.println(productName + " " + data + " " + overdue);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if(resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignore) {}
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignore) {}
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignore) {}
            }
        }
    }
}
