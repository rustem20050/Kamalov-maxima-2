import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class MainGood {

    //language=SQL
    private static final String SQL_SELECT_FROM_ACCOUNT = "select * from foodstuffs order by id";

    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_ACCOUNT)) {
                while (resultSet.next()) {
                    String productName = resultSet.getString("productname");
                    String data = resultSet.getString("receiptdate");
                    boolean overdue = resultSet.getBoolean("overdue");

                    System.out.println(productName + " " + data + " " + overdue);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
